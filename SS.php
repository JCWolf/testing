
<?php 

/**
* the SS class
*/
interface Extension{

}
class SS implements Extension{

	// pattern types
	const PHP 		= "php";
	const SNIPPET 	= "snippet";
	const SECTION 	= "section";

	private 
		$regex = [
			'snippet' => "/#([a-zA-Z][\w]+?) *?{{[\s\r\t\n]*(.+?)[\s\r\t\n]*?}}(?=\s*\#|;)/is"
		];
	private static 
		$patterns = [],
		$snippets = [];

	public function __construct($file) {
		// get file data
		$data = file_get_contents($file).";"; 

		// parse snipp
		$data = $this->parse($data);

		self::$snippets = $data;
	}

	public function parse($data = ""){
		$snippets = [];
		// get regex matches
		preg_match_all($this->regex['snippet'], $data,$matches);
		// assign snippets to snippets var
		foreach ($matches[1] as $id => $snippet)
			$snippets[$snippet] = $matches[2][$id];

		return $snippets;
	}

	public static function get($name){
		return self::$snippets[$name];
	}
	public static function usePattern($pattern,$snippet,$data){
		$func = self::$patterns[$pattern];
		return $func($snippet,$data);
	}

	public static function pattern($name,$func){
		self::$patterns[$name] = $func;
	}

	public function __call($closure, $args){
		// use patterns
		$snippet = self::$snippets[$closure];
		foreach (self::$patterns as $func) {
			$snippet = $func($snippet,$args);
		}
		echo $snippet;
	}

}


SS::pattern(SS::PHP,function($snippet,$data = []){
	foreach ($data[0] as $key => $val) {
		if(!is_array($val))
			// single value
			$snippet = str_replace("$".$key, $val, $snippet);
		else{
			// array value
			$snippetVal = $key.".";
			foreach ($val as $k => $v){
				$snippetVal.= $k.".";
				$snippetVal = substr($snippetVal, 0, -1);
				if (strpos($snippet,$snippetVal) === false)
					continue;

				$snippet = str_replace("$".$snippetVal, $v, $snippet);

                $snippetVal = $key.".";             
            }         
        }     
    }
	return $snippet; 
}); 
function sortArrayByArray(Array $array, Array $orderArray) {
    $ordered = array();
    foreach($orderArray as $key) {
        if(array_key_exists($key,$array)) {
            $ordered[$key] = $array[$key];
            unset($array[$key]);
        }
    }
    return $ordered + $array;
}
SS::pattern(SS::SNIPPET,function($snippet,$data){
	$regex = '/%([a-zA-Z][a-zA-Z0-9]+)\[?([a-zA-Z0-9]+)?\]?/i';

	$snippets = [];

	preg_match_all($regex, $snippet,$matches);

	arsort($matches[0]);
	foreach ($matches[0] as $key => $value) {
		$string 		= "";
		$snippetData 	= $matches[1][$key];
		$dataVal 		= empty($matches[2][$key]) ? $matches[1][$key] : $matches[2][$key];

		// varaible is not set in the data array (remove ss syntax)
		if(!isset($data[0][$dataVal])) {

			// $snippet = str_replace($value, "", $snippet);
			continue;
		}

		$sectionData =  $data[0][$dataVal];
		$snippetData =  SS::get($snippetData);

		// check if snippet has data
		if(!isset($data[0][$dataVal])){
			$snippets[$matches[1][$key]] = $matches[0][$key]; continue;
		}

		foreach ($sectionData as $sectionDataValue) 
			$string .= SS::usePattern(SS::PHP,$snippetData,[$sectionDataValue]);

		$snippet = str_replace($matches[0][$key], $string, $snippet);

		// remove data from matches
		unset($matches[0][$key],$matches[1][$key],$matches[2][$key]);

	}

	foreach ($snippets as $key => $value) {
		$snip = SS::get($key);
		$snippet = str_replace($value, $snip, $snippet);
	}

	// clear the snippet from invalid SS syntax
	arsort($matches[0]);
	foreach ($matches[0] as $key => $value) {
		$snippet = str_replace($value, "", $snippet);
	}
	return $snippet;
});

SS::pattern(SS::SECTION,function($snippet,$data){
	$regex = '/%\[ *?(\w+?) *?\]{{[\s\t\n\r]*(.*?)?[\s\t\n\r]*?}}/is';

	preg_match_all($regex, $snippet,$matches);


	foreach ($matches[0] as $key => $value) {
		$dataVal = $matches[1][$key];

		// check if section has data
		if(!isset($data[0][$dataVal]))
			continue;

		$sectionData =  $data[0][$dataVal];
		$snippetData =  $matches[2][$key];

		$string = "";

		foreach ($sectionData as $sectionDataValue) 
			$string .= SS::usePattern(SS::PHP,$snippetData,[$sectionDataValue]);
		$snippet = str_replace($matches[0][$key], $string, $snippet);

	}

	return $snippet;
})

?>

<?php 

function __autoload($class_name) {
	include $class_name . '.php';
}

function pre(){
	echo "<pre>".PHP_EOL;
	foreach (func_get_args() as $arg) {
		print_r($arg);}
	echo PHP_EOL."</pre>".PHP_EOL;
}

// new Language("Romanian");
new Language();

$S = new SS("htmls.html");
$data = [
		"buildName"	=> "towncenter",
		"level"		=> 11,
		"time" 		=> time(),
		// "id" 		=> 222222,
		"user" 		=> [
					"name" 	=> "John",
					"age"	=> 20
				],
		"items" 	=> [
					[
						"id"	=> 6,
						"name"	=> "4567457 1"
					],
					[
						"id"	=> 7,
						"name"	=> "4567457 2"
					],
					[
						"id"	=> 8,
						"name"	=> "4567457 3"
					],
				],
		"listItem" 	=> [
					[
						"id"	=> 1,
						"name"	=> "some name 1"
					],
					[
						"id"	=> 2,
						"name"	=> "some name 2"
					],
					[
						"id"	=> 3,
						"name"	=> "some name 3"
					],
				]
	];

$S->elem($data);
		
$time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];


echo "Process time: $time seconds\n";
?>